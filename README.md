<h1 align="center">i18n-translator</h1>

<img alt="Project banner" src="docs/banner.jpg">

<p>
  <a href="https://www.npmjs.com/package/i18n-translator" target="_blank">
    <img alt="Version" src="https://img.shields.io/npm/v/i18n-translator.svg">
  </a>
  <a href="#" target="_blank">
    <img alt="License: GNU GPLv3" src="https://img.shields.io/badge/License-GNU GPLv3-yellow.svg" />
  </a>
</p>

> This is a utility providing an interface for zero-config import, modification and export of vue-18n language files as well as Laravel localization files. Typescript support for Vue provided with `vue-property-decorator`.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your unit tests
```
npm run test:unit
```

## Author

👤 **Ivan Kotlaja**

* Website: https://ivnk.dev